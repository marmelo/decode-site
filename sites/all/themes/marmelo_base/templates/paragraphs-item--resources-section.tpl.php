<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened into
 *   a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <div class="content"<?php print $content_attributes; ?>>    

    <?php

        $views_numbers = views_get_view_result('resources', 'default', $content['field_what_type_']['#items'][0]['tid']);

        if(count($views_numbers)) {
            $views_output = views_embed_view('resources', 'default', $content['field_what_type_']['#items'][0]['tid']);

    ?>

        <h2><a name='<?= str_replace(" ", "-", trim(strtolower($content['field_heading'][0]['#markup']))); ?>' ><?php print render($content['field_heading']); ?></a></h2>

        <?php 
            print $views_output;
        ?>

    <?php
        }
    ?>
  </div>
</div>
