<?php

/*
    Marmelo base theme for Drupal
    Copyright (C) 2017  Marmelo Ltd
 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 

    */


/** Preprocess functions for the Marmelo base theme **/

function marmelo_preprocess_html(&$variables) {

    drupal_add_css('https://fonts.googleapis.com/css?family=Montserrat:300,400,500', array('type' => 'external'));

}

function marmelo_preprocess_page(&$variables) {

    // change logo to use the svg file in this folder
    $variables['logo'] = "/".drupal_get_path('theme', 'marmelo')."/img/logo.svg";

}


function marmelo_preprocess_node(&$variables) {

    // add a drop down that lets people skip to later sections on basic pages
    if($variables['view_mode'] == 'full' && $variables['type'] == 'page' && isset($variables['field_sections']) && count($variables['field_sections']) > 0 && isset($variables['body'])) {


        if(isset($variables['field_show_skipper']['und'][0]['value']) && $variables['field_show_skipper']['und'][0]['value'] == 1) {


            if(count($variables['field_sections'] > 1)) {


                // get list of sections from field_sections
                // TODO: cache this, or store with node
                foreach($variables['field_sections'] as $section) {
                        $ids[] = $section['value'];
                }
                $items = field_collection_item_load_multiple($ids);

                $sections = array();
                $link_markup = "<div class='section-skipper'><label for='skipto'>Skip to:</label><select name='skipto' id='skipto'>";
                $count = 0;
                foreach($items as $delta => $section_item) {

                    $this_item = entity_metadata_wrapper('field_collection_item', $section_item);
                    $sections[] = array( 
                        'title' => $this_item->field_title->value(),
                        'id' => "section_".$count
                    );

                    $link_markup .= "<option value='$count'>".$this_item->field_title->value()."</option>";
                    $variables['content']['field_sections'][$count]['#attributes']['id'] = "section_".$count;
                    

                    $count++;

                }
                $link_markup .= "</select></div>";


                $variables['content']['skipper'] = array(

                    0 => array(
                        '#markup' => $link_markup
                    ),
                    '#weight' => 0.5
                    
                );

            }


            
        }


    }

    if($variables['view_mode'] == 'full' && $variables['type'] == 'resources_page' && isset($variables['field_resource_sections']) && count($variables['field_resource_sections']) > 0) { 

        $ids = [];
        $variables['views'] = [];
        foreach($variables['field_resource_sections'] as $section) {
            $ids[] = $section['value'];
        }

        $items = entity_load('paragraphs_item',$ids);        
    
        $link_markup = "<ul>";
        foreach($items as $item) {

            // do we have any results?

            $views_numbers = views_get_view_result('resources', 'default', $item->field_what_type_['und'][0]['tid']);

            if(count($views_numbers)) {

                $item_text = $item->field_heading['und'][0]['value'];
                $safe_item_text = str_replace(" ", "-", trim(strtolower($item_text)));
                $link_markup .= "<li><a href='#$safe_item_text'>$item_text</a></li>";

            }

        }
        $link_markup .= "</ul>";

        $variables['content']['skipper'] = array(

            0 => array(
                '#markup' => $link_markup
            ),
            '#weight' => 0.5
            
        );
    }


    // display the title 'blog' for all blogs
    if($variables['type'] == 'article' && $variables['view_mode'] == 'teaser') {
        $variables['title'] = "Blog";
        
    }

    
    if($variables['type'] == 'resource') {        
      //  $variables['mediatype'] = $variables['field_category'];
    }
    

}

function marmelo_preprocess_field(&$vars, $hook) {

    if($vars['element']['#field_name'] == "field_timeline_point" || $vars['element']['#field_name'] == "field_year" ) {
        foreach($vars['items'] as $key => $item) {
        //   $vars['items'][$key]['#test'] = "Hello";
        }
    }

}


function human_filesize($bytes, $decimals = 2) {
    $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
    $factor = floor((strlen($bytes) - 1) / 3);
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
  }