<?php

/*
    Marmelo base theme for Drupal
    Copyright (C) 2017  Marmelo Ltd
 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 

    */
/**
 * @file node.tpl.php
 * Customised version of the standard Drupal node template
 */

$fileicons = [
  'application/msword' => 'file-word',
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'file-word',
  'application/vnd.oasis.opendocument.presentation' => 'file-powerpoint',
  'application/vnd.oasis.opendocument.spreadsheet' => 'file-excel',
  'application/vnd.oasis.opendocument.text' => 'file-word',
  'application/pdf' => 'file-pdf',
  'application/vnd.ms-powerpoint' => 'file-powerpoint',
  'application/vnd.openxmlformats-officedocument.presentationml.presentation' => 'file-powerpoint',
  'application/vnd.ms-excel' => 'file-excel',
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'file-excel',
  'application/zip' => 'file-archive',
  'generic' => 'file'
];

?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print $user_picture; ?>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="submitted">
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      
      $media_set = isset($content['field_media']['#items'][0]) ? $content['field_media']['#items'][0] : false;
      if($media_set) {
        $link = isset($content['field_media']['#items'][0]['uri']) ? $content['field_media']['#items'][0]['uri'] : false;
        $size = isset($content['field_media']['#items'][0]['filesize']) ? $content['field_media']['#items'][0]['filesize'] : false;
        $type = isset($content['field_media']['#items'][0]['type']) ? $content['field_media']['#items'][0]['type'] : false;
        $mime = isset($content['field_media']['#items'][0]['filemime']) ? $content['field_media']['#items'][0]['filemime'] : false;
        $preview = isset($content['field_media'][0]['file']['#preview']) ? $content['field_media'][0]['file']['#preview'] : false;
        $iconclass = isset($fileicons[$mime]) ? $fileicons[$mime] : $fileicons['generic']; 
        $localfile = $size > 0;
      }
      
      if($media_set && (!$localfile || $type=="image")) {
        // render media (the video)
        print render($content['field_media']);
        hide($content['field_image_thumb']);
      } elseif(isset($content['field_image_thumb'])) {
        print render($content['field_image_thumb']);
        hide($content['field_media']);
      } else {
        if($iconclass) {
          echo("<div class='fileicon-container'><i class='fas fa-".$iconclass."'></i></div>");
        }
        hide($content['field_media']);      
      }

  ?>
    <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
    <p class='date'><?php echo(format_date($variables['created'], 'short_human'));  ?></p>
      <?php
      hide($content['comments']);
      hide($content['links']);
      print render($content);
      // if there's a file, provide a download link
      if(isset($content['field_media']['#items'][0])) {
        if(isset($content['field_media']['#items'][0]['uri'])) {

          // is it actually a downloadable file though?  Needs to have a filesize > 0
          if(($size = $content['field_media']['#items'][0]['filesize']) > 0) {
            $link = file_create_url($content['field_media']['#items'][0]['uri']);
            print("<div class='download-link'><a href='".$link."'>Download file</a> (".human_filesize($size).")</div>");  
          }

        }
      }

      
    ?>
  </div>

  <?php print render($content['links']); ?>

  <?php print render($content['comments']); ?>

</article>
