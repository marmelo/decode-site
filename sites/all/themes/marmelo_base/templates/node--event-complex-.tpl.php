<?php

/*
    Marmelo base theme for Drupal
    Copyright (C) 2017  Marmelo Ltd
 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 

    */
/**
 * @file node.tpl.php
 * Customised version of the standard Drupal node template
 */

?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print $user_picture; ?>  

    <?php hide($content['field_top_image']); ?>

  <div class='img-bg' style='background-image: url("<?php echo file_create_url($node->field_top_image['und'][0]['uri']); ?>");"'>
    <div class='event-overlay'>


        <?php
          print render($content['field_logos']);
        ?>

        <h2 class='event-title'><?php print $title; ?></h2>

        <div class='event-details'>
            <?php
              print render($content['field_date_text']);
              print render($content['field_location_text']);
              
            ?>
        </div>

        <?php
          print render($content['field_register_link']);
          //print render($content['group_details']);
          print render($content['field_sub_pages']);

        ?>
    </div>
  </div>

    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      
      print render($content);
    ?>
  </div>


</article>
