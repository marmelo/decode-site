/*
    Marmelo base theme for Drupal
    Copyright (C) 2017  Marmelo Ltd
 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 

    */

/**
 * @file DecodeDrawer
 * Draws coloured squares onto the screen in random places at random intervals
 */


var DecodeDrawerOptions = {

    colors: ['#e4701e', '#33e986', '#fff16e', '#fff00ff', '#00b1b0', '#E37FD2']

};

function DecodeDrawer(canvasId, options) {

    if(!options) {
        options = {};
    }

    /*  MEMBER VARIABLES  */
    this.canvas = null;     // I know this isn't necessary, but I like to have my properties up top
    this.canvasContext = null;

    this.options = {
        timeBetweenPatterns: options.timeBetweenPatterns ? options.timeBetweenPatterns : 600,
        timeBetweenSquares: options.timeBetweenSquares ? options.timeBetweenSquares : 200,
        debugMode: options.debugMode ? options.debugMode : false,
        limitFrames: options.limitFrams ? options.limitFrames : 0,
        maxPatterns: options.maxPatterns ? options.maxPatterns : 10 
    };


    this.timers = {
        nextSquareTime: 0,
        nextPatternTime: 0
    };

    this.drawrects = options.drawrects ? options.drawrects : {x:0, y:0, w: 100, h:100};

    this.activePatterns = new Array();
    this.completePatterns = new Array();
    this.patternsForDeletion = new Array();

    this.activePatternCount = 0;

    this.lastRender = 0;
    this.thisRender = 0;
    this.called = 0;    

    this.lastDrawRect = null;

    // TODO: pass these from the theme


    /*  HELPER OBJECTS */
    this.Pattern = function(drawrect) {

        this.completeSquares = new Array();
        this.activeSquares = new Array();

        this.drawRect = drawrect ? drawrect : {

            x: 0,
            y: 0,
            w: 100,
            h: 100

        };

        this.outOfBounds = false;

        this.Square = function(x,y,color) {

            this.x = x;
            this.y = y;
            this.renderStep = 0;
            this.renderComplete = false;
            this.color = color;

            return this;
        }

        this.addSquare = function() {

            var x = 0;
            var y = 0;
            var color = DecodeDrawerOptions.colors[Math.floor(Math.random() * DecodeDrawerOptions.colors.length)];


            // determine x and y coordinates
            if(this.completeSquares.length) {

                var mostRecentSquare = this.completeSquares[this.completeSquares.length -1];

                // let's add this square to the existing ones

                // put the new one near the last one
                x = Math.random() > 0.1 ? mostRecentSquare.x + 10 : mostRecentSquare.x;
                y = Math.random() > 0.9 ? mostRecentSquare.y + 10 : mostRecentSquare.y;
                
                // 30% chance it just takes the last one's colour                
                if(Math.random() < 0.3) {
                    color = mostRecentSquare.color;
                }

            } else {

                // first square, so put it anywhere we like
                x = this.drawRect.x + (Math.round((Math.random() * (this.drawRect.w)) / 10) * 10);
                y = this.drawRect.y + (Math.round((Math.random() * (this.drawRect.h)) / 10) * 10);

            }

            // do a quick check to make sure this Square doesn't break bounds
            if(x > this.drawRect.x + this.drawRect.w || y > this.drawRect.y + this.drawRect.h) {

                this.outOfBounds = true;

            } else {

                this.activeSquares.push(new this.Square(x, y, color));

            }


        }

        this.markSquareComplete = function(index) {
            this.completeSquares.push(this.activeSquares[index]);
            this.activeSquares.splice(index, 1);
        }


        return this;

    };    


    /*  METHODS */

    /*  Get canvas once initialised; fail if canvas element not available  */
    if(canvasId) {

        this.canvas = document.getElementById(canvasId);

        if(this.canvas) {

            this.canvasContext = this.canvas.getContext("2d");

            if(!this.canvasContext) {
                return false;
            }

        } else {
            this._debug("No canvas element available with that ID");
            return false;
        }
        
    }

    /*  Apply functions and start timer */
    this.start = function() {

        this.scaleCanvas();

        // ask the browser to let us know when it's next ready to render
        window.requestAnimationFrame(this.render.bind(this));
        

    };

    this._debug = function(message) {

        if(this.options.debugMode) {
            console.log(message);
        }

    }


    /*  Renders based on current state each frame */
    this.render = function(timestamp) {

        this.called++;
        this._debug(this.called);
        this.thisRender = timestamp;

        if(timestamp > this.timers.nextPatternTime || this.activePatterns.length === 0) {

            // time to draw a new pattern                    
            var drawRectIndex = null;

            switch(this.drawrects.length) {

                case 1:
                    drawRectIndex = 0;
                    break;
                case 2:
                    drawRectIndex = this.lastDrawRect == 1 ? 0 : 1;
                    break;
                default:
                    // Don't draw in the same rect as last time if there's more than one available
                    // these are both initialised to null so the first time it will pass as soon as
                    // a valid integer has been found
                    while(drawRectIndex == null || drawRectIndex == this.lastDrawRect) {  
                        drawRectIndex = Math.floor(Math.random() * this.drawrects.length);
                    }
                    break;

            }


            this.activePatterns.push(new this.Pattern(this.drawrects[drawRectIndex]));
            this.timers.nextPatternTime = timestamp + this.options.timeBetweenPatterns;
            this.activePatternCount++;
            this.lastDrawRect = drawRectIndex;
            this._debug("New pattern");

        }

        this.updatePatterns();

        this.thisRender = 0;
        this.lastRender = timestamp;

        if(this.options.limitFrames == 0 || this.called < this.options.limitFrames) {
            window.requestAnimationFrame(this.render.bind(this));
        }

    };


    /*  Add squares to patterns or mark them ended */
    this.updatePatterns = function() {


        var markedForDeletion = new Array(); 

        // First of all, are there any active patterns?
        if(this.activePatternCount) {

            // there are?  Great
            for(var i = 0; i < this.activePatternCount; i++) {
                                            
                // add a square, if it's time
                if(this.thisRender > this.timers.nextSquareTime) {
                    //this._debug("Adding square");
                    this.activePatterns[i].addSquare();
                }

                try {
                    this.updateSquares(this.activePatterns[i]);                    
                }

                catch(e) {
                    console.log(this.activePatterns);
                    console.log(i);
                }


                // 0.1% chance of this pattern coming to an end
                if(this.activePatterns[i].activeSquares.length === 0 &&  (Math.random() < 0.028 || this.activePatterns[i].outOfBounds)) {
                    markedForDeletion.push(i);
                    //this._debug("Pattern is over!");
                }

            }


        }


        var markedForPermanentDeletion = new Array();
        // Paint over all the passages for deletion
        if(this.patternsForDeletion) {

            for(var k = 0; k < this.patternsForDeletion.length; k++) {
                                            
                // no more squares left, delete this pattern completely
                if(!this.patternsForDeletion[k].completeSquares.length) {
                    markedForPermanentDeletion.push(k);
                } else {
                    this.paintOverSquares(this.patternsForDeletion[k]);                                    
                }

            }

        }        

        // permanently remove patterns that are no longer needed
        if(markedForPermanentDeletion.length) {

            for(var l = markedForPermanentDeletion.length -1; l > -1; l--) {
                markedForPermanentDeletion.splice(l, 1);
            }

        }

        // remove any that have been marked for deletion
        // we have to do this separately to avoid messing up the array while we're in it
        if(markedForDeletion.length) {

            for(var j= markedForDeletion.length - 1; j > -1; j--) {

                this.completePatterns.push(this.activePatterns[markedForDeletion[j]]);                
                this.activePatterns.splice(markedForDeletion[j], 1);
                this.activePatternCount--;

            }

        }


        if(this.thisRender > this.timers.nextSquareTime) {
            this.timers.nextSquareTime = this.thisRender + this.options.timeBetweenSquares;       
        }

        // once we get to ten complete patterns, time to start deleting them
        if(this.completePatterns.length > this.options.maxPatterns) {

            // delete the last pattern
            this.patternsForDeletion.push(this.completePatterns[0]);
            this.completePatterns.splice(0,1);

        }

    };


    /*  Draw updated squares  */
    this.updateSquares = function(pattern) {

        // Do we have any squares that haven't finished rendering yet?

        var markComplete = new Array();

        // If so, draw the next step of their animation
        this._debug("Updated squares");
        if(pattern.activeSquares.length) {
            for(i = 0; i < pattern.activeSquares.length; i++) {
                
                switch(pattern.activeSquares[i].renderStep) {

                    case 0:
                        pattern.activeSquares[i].renderStep++;
                        this.drawSquare(pattern.activeSquares[i], 1);
                        break;

                    case 1:
                        pattern.activeSquares[i].renderStep++;
                        this.drawSquare(pattern.activeSquares[i], 2);
                        break;

                    case 2:
                        pattern.activeSquares[i].renderStep++;
                        this.drawSquare(pattern.activeSquares[i], 4);
                        break;

                    case 3:
                        pattern.activeSquares[i].renderStep++;
                        this.drawSquare(pattern.activeSquares[i], 6);
                        break;

                    case 4:
                        pattern.activeSquares[i].renderStep++;
                        this.drawSquare(pattern.activeSquares[i], 8);
                        break;

                    case 5:                    
                        this.drawSquare(pattern.activeSquares[i], 10);                    
                        markComplete.push(i);
                        break;

                }
                
            }
        }

        for(var j = markComplete.length -1; j > -1; j--) {
            
            pattern.markSquareComplete(markComplete[j]);                

        }
                        

    };

    this.paintOverSquares = function(pattern) {

            try {

                pattern.completeSquares[0].color = "#FFFFFF";

            }

            catch(e) {
                console.log(pattern.completeSquares);
            }


            switch(pattern.completeSquares[0].renderStep) {

                case 5:
                    pattern.completeSquares[0].renderStep--;
                    this.drawSquare(pattern.completeSquares[0], 1);
                    break;

                case 4:
                    pattern.completeSquares[0].renderStep--;
                    this.drawSquare(pattern.completeSquares[0], 2);
                    break;

                case 3:
                    pattern.completeSquares[0].renderStep--;
                    this.drawSquare(pattern.completeSquares[0], 4);
                    break;

                case 2:
                    pattern.completeSquares[0].renderStep--;
                    this.drawSquare(pattern.completeSquares[0], 6);
                    break;

                case 1:
                    pattern.completeSquares[0].renderStep--;
                    this.drawSquare(pattern.completeSquares[0], 8);
                    break;

                case 0:                    
                    pattern.completeSquares[0].renderStep--;
                    this.drawSquare(pattern.completeSquares[0], 10);
                    pattern.completeSquares.splice(0,1);
                    break;

            }

    }


    /*  Ensure canvas element painting area matches full screen 
        This is called on start, but can also be called on resize 
        as needed  */
    this.scaleCanvas = function() {
        this.canvas.width = document.body.clientWidth;
        this.canvas.height = document.body.clientHeight;                
    }



    /*  Draw a square based on current status */
    this.drawSquare = function(square, size) {

        if(size > 10) {

            // must be smaller than 10px wide
            return false;

        } else {

            if(square.color == "#FFFFFF") {

                this.canvasContext.clearRect(square.x, square.y, size, size);

            } else {
                var border = 10 - size;

                this.canvasContext.fillStyle = square.color;
                this.canvasContext.fillRect(square.x + border, square.y + border, size, size);
            }
            
        }

    };


    return this;

}