/*
    Marmelo base theme for Drupal
    Copyright (C) 2017  Marmelo Ltd
 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 

    */
/**
 * @file theme.js
 * Basic click handlers for hamburger and skip to
 */


(function($) {

    $(document).ready(function() {


        $('#skipto').on('change', function() {
            
            $(window).scrollTo($('#section_'+$('#skipto option:selected').attr('value')), { duration: 200 });

        });


        $('button.hamburger').on('click', function() {

            $('button.hamburger').toggleClass('is-active');
            $('nav').toggleClass('show');

        });


        
        $('.field-name-field-highlights').marmeloSlide('> .field-items', '.field-item', 1);

        $('body.node-type-event-sub-page .paragraphs-items-field-sections-sub-page .paragraphs-item-speaker .field-name-field-biog').readmore({
            collapsedHeight: 0
        });

        $('a[href*="#"]').on('click', function (e) {
            e.preventDefault();

            var id = $(this).attr('href');
            id = id.substring(1,id.length);

            console.log('*[name="'+id+'"]');
        
            $('html, body').animate({
                scrollTop: $('*[name="'+id+'"]').offset().top
            }, 500, 'linear');
        });

    });

})(jQuery);