/*
    Marmelo base theme for Drupal
    Copyright (C) 2017  Marmelo Ltd
 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 

    */
/**
 * @file canvas.js
 * Sets up a canvas in the background to draw blocks
 */


(function($) {

    $(document).ready(function() {


        var avoidWidth = document.getElementById('page-wrapper').clientWidth;
        var availableWidth = ((document.body.clientWidth - avoidWidth) / 2);

        if(availableWidth > 10) {
            var draw = new DecodeDrawer('background-canvas', 
                {   
                    debugMode: false, 
                    drawrects: [ {
                            x: 0,
                            y: 100,
                            w: Math.round(availableWidth - 70),
                            h: Math.round(document.body.clientHeight -100)
                        }, {
                            x: Math.round(availableWidth + avoidWidth + 70),
                            y: 100,
                            w: Math.round(availableWidth - 70),
                            h: Math.round(document.body.clientHeight - 100)
                        }]
                });
            draw.start();

        } else {

            console.log("We regret to inform you there is no space to draw sideblocks.");

        }



    });

})(jQuery);