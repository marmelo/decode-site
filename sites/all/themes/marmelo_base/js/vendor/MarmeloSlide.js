(function($) {

    $.fn.marmeloSlide = function(containerName, itemClass, minNumber) {
    

        console.log("Creating marmelo slider with container "+ containerName + " and " + itemClass);

        this.find(containerName).addClass('marmelo-slide-container');
        this.find(containerName + ">" + itemClass).addClass('marmelo-slide-item');

        // assign this slider a unique classname
        var date = new Date();
        var uniqueClass = "uq-" + date.getMilliseconds() + Math.round(Math.random() * 999999);
        var uniqueClassId = uniqueClass + "-item";
        this.addClass(uniqueClass).addClass('marmelo-slide-loaded');        
        this.find(containerName + " > " + itemClass).first().addClass('show');        

        minNumber = minNumber ? minNumber : 1;

        if(this.find(containerName + " > " + itemClass).length < (minNumber+1)) {
            console.log("Not enough to carouse");
            return this;
        }

        // add paging
        pag = $("<div class='pagination'></div>");
        i = 0;        
        
        this.find(containerName + " > " + itemClass).each(function() {

            $(this).addClass(uniqueClassId);

            thislink = $("<i data-link-to='" + i + "' data-parent-class='" + uniqueClass + "' class='paginate-link'></i>");
            thislink.on('click', function() {


                allItems = $("."+$(this).attr('data-parent-class')).find(containerName + " " + itemClass);                
                allItems.removeClass('show').addClass('past');
                refItem = $(allItems.get($(this).attr('data-link-to')));
                refItem.removeClass('past').addClass('show');
                $(this).addClass('show');
                
            });
            i++;
            
            pag.append(thislink);                
        });
        this.append(pag);        
    
        // add next/prev
        prev = $("<div class='previous'></div>");
        next = $("<div class='next'></div>");
        this.prepend(prev);
        this.append(next);
    
        // set behaviours
        prev.on('click', function() {
            
            var allItems = this.find(containerName + " > " + itemClass);

            // if fewer than minimum would be on screen, show all
            if(allItems.not('.past').length <= minNumber) {
                allItems.removeClass('past').removeClass('show');            
            } else {
                
                current = this.find(containerName + " > " + itemClass+".show");
                if(current.length < 1) {
                    current = allItems.first();
                }

                current.removeClass('show').addClass('past');
                var nextOne = current.prev();
                if(nextOne.length < 1) {
                    allItems.removeClass('past');
                    allItems.first().addClass('past');
                    nextOne = allItems.last(); 
                }
                nextOne.removeClass('past').addClass('show')
            }            

        }.bind(this));

        next.on('click', function(e, passed) {

            if(!passed) {
                // set up another movement
                $(this).data('paused', 'true');
            }

            if($(this).data('paused') && passed) {

            } else {
                var allItems = this.find(containerName + " > " + itemClass);

                // if fewer than minimum would be on screen, show all
                if(allItems.not('.past').length <= minNumber) {
                    allItems.removeClass('past').removeClass('show');            
                } else {
                    
                    current = this.find(containerName + " > " + itemClass+".show");
                    if(current.length < 1) {
                        current = allItems.first();
                    }
    
                    current.removeClass('show').addClass('past');
                    var nextOne = current.next();
                    if(nextOne.length < 1) {
                        allItems.removeClass('past');
                        nextOne = allItems.first(); 
                    }
                    nextOne.removeClass('past').addClass('show')
                }                
            }
            
 
        }.bind(this));
        

        return this;
    
    }
    
})(jQuery);
