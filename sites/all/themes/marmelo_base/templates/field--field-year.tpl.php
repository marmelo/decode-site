<?php

/*
    Marmelo base theme for Drupal
    Copyright (C) 2017  Marmelo Ltd
 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 

    */

/**
 * @file field--field-year.tpl.php
 * Customised version of the standard Drupal field template
 */

    

?>

<?php foreach ($items as $delta => $item): ?>


    <?php

        $styletext = "";
        if(isset($item['entity']['field_collection_item'])) {

            foreach($item['entity']['field_collection_item'] as $key => $fc_item) {
                if(isset($fc_item['field_position'][0]['#markup'])) {
                    $styletext = "style='left: ".round($fc_item['field_position'][0]['#markup'])."%;'";
                }
            }

        }

        

    ?>

    <div <?php echo($styletext); ?> class="field-item <?php print $delta % 2 ? 'odd' : 'even'; ?> <?php print $classes; ?>"<?php print $item_attributes[$delta]; ?>><?php print render($item); ?></div>
<?php endforeach; ?>